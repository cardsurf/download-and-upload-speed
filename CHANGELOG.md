# Changelog

### Version 1.6.4
Added a setting that determines a minimum download value displayed in the panel  
Added a setting that determines a minimum upload value displayed in the panel  

### Version 1.6.3
Added a setting that determines how often to update a list of available network interfaces  
Added functionality to automatically select a network interface after being plugged back into a computer  
Added Italian translation (thanks [Dragone2](https://github.com/Dragone2))  

### Version 1.6.2
Added an option to select order of values shown in a panel and in a hover popup  
Set minimum calculation interval to 0.1s  
Added Hungarian translation (thanks [KAMI911](https://github.com/KAMI911))  

### Version 1.6.1
Replaced tempfile command with mktemp

### Version 1.6.0
Added support for Cinnamon 3.8  
Added a feature to show a sum of download and upload speed values when multiple network interfaces are checked  
Added Polish translation (thanks [xearonet](https://github.com/xearonet))  
Added German translation (thanks [NikoKrause](https://github.com/NikoKrause))  

### Version 1.5.0
Added support for Cinnamon 3.4  
A list of available network interfaces updates when a context menu opens  
Added scripts that list current Internet connections  
Fixed a bug of invoking a data limit exceeded command after applet start when data limit was not set  
Fixed an applet size when user interface scaling was set to Hi-DPI  
Added Danish translation (thanks [Alan01](https://github.com/Alan01))  
Added Bulgarian translation (thanks [spacy01](https://github.com/spacy01))  

### Version 1.4.0
Added translation support (thanks [eson57](https://github.com/eson57), [giwhub](https://github.com/giwhub))  
Added Swedish translation (thanks [eson57](https://github.com/eson57))  
Added Chinese translation (thanks [giwhub](https://github.com/giwhub))  
Added Croatian translation (thanks [muzena](https://github.com/muzena))  
Fixed an issue that prevented to set custom icons in Cinnamon 3.2  
Fixed a bug that caused a hover popup to be displayed after Cinnamon restart regardless of configuration settings

### Version 1.3.0
Added an option to display total amount of data downloaded and uploaded from the custom date  
Added an option to display average speed per second or amount of data transferred  
Added an option to display values as multiples of bytes or bits  
Updated configuration sections  

### Version 1.2.0
Added setting the data limit  
Added the indicator of data limit usage  
Added the command invocation when the data limit exceeded  
Added setting the number of decimal places of download and upload speed displayed in the panel

### Version 1.1.0
Added text styling with CSS  
Added customization of icons displayed in the panel

### Version 1.0.0
Initial release

# Download and upload speed
An applet that shows usage of a network interface for Linux Mint Cinnamon

![](/screenshots/compact.png)

## Features
* Shows current download and upload speed
* Shows a hover popup with total amount of data downloaded and uploaded
* Opens a terminal with list of current Internet connections on a left mouse click.  
  Closes the terminal on the next mouse click.
* Shows data limit usage
  * Displays a percentage of data limit usage as a circle or text
  * Invokes a command when the data limit exceeded
* Customizable
  * Two GUI types
  * Show average speed per second or amount of data transferred
  * Display values as multiples of bytes or bits  
  * Enable or disable a hover popup
  * Customize a command used to list current Internet connections

## Installation
1. Extract .zip archive to ~/.local/share/cinnamon/applets
2. Enable the applet in Cinnamon settings

## Usage
To specify a network interface:  
  
1. Right click on the applet
2. From "Network interfaces" submenu check a network interface